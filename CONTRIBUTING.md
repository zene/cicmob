# CONTRIBUTING


## Dependencies

We assume you have already installed the dependencies written on [README.md](README.md).


## Create a dev environment  (on debian based distros)


### SSH Key

Please set on gitlab your public ssh-key in the profile configuration area.


### Execute our dev-env script

`curl -s https://gitlab.com/cooperativa-integral-catalana/cicmob/raw/master/dev-stuff/install-dev-env.sh | sudo sh`


### Config your git

```
sudo su - cicmob  # enter the new environment
git config --global user.name MY_AWESOME_USERNAME  # preferibly same as your gitlab user
git config --global user.email MY_AWESOME_EMAIL
```

## Testing policy

Every new feature should include respective unit tests.


## Merging policy

Only fast-forward merges from separate branches.

