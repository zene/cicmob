#!/bin/sh

MY_USER=$1

set -e

echo "Creating PostgreSQL database..."

/bin/su postgres -c "psql postgres -c 'create database dj_cicmob;'"

echo "[DONE]"

echo "Creating PostgreSQL user..."

/bin/su postgres -c "psql postgres -c 'create user cicmob;'"

echo "[DONE]"

echo "Granting db privileges to user..."

/bin/su postgres -c "psql postgres -c 'GRANT ALL PRIVILEGES ON DATABASE dj_cicmob TO cicmob;'"

echo "[DONE]"

echo "Creating dedicated system user and group..."

/usr/sbin/adduser cicmob --gecos "cicmob cicmob, bla, bla, bla" --disabled-password

echo "[DONE]"


echo "Copying ssh setup from home folder..."

/bin/cp -r /home/$MY_USER/.ssh /home/cicmob/
/bin/chown -R cicmob:cicmob /home/cicmob/.ssh

echo "[DONE]"


echo "Cloning the git repo..."

/bin/su cicmob -c "cd; git clone git@gitlab.com:cooperativa-integral-catalana/cicmob.git"

echo "[DONE]"

echo "Creating the virtualenv..."

/bin/su cicmob -c "cd; pyvenv /home/cicmob/venv/"

echo "[DONE]"

echo "Automatically activate virtualenv..."

echo "source venv/bin/activate" >> /home/cicmob/.bashrc

echo "[DONE]"

echo "Automatically load Django environment variables..:"

echo "export DJANGO_SETTINGS_MODULE='fonny.settings.development'" >> /home/cicmob/.bashrc
echo "export STATIC_ROOT='/home/cicmob/static'" >> /home/cicmob/.bashrc
echo "export MEDIA_ROOT='/home/cicmob/media'" >> /home/cicmob/.bashrc

echo "[DONE]"

echo "Installling project requirements..."

/bin/su cicmob -c "cd; source venv/bin/activate; pip3 install --upgrade -r cicmob/requirements.txt"

echo "[DONE]"

echo "Changing permissions and owner..."

/bin/chown -R cicmob:cicmob /home/cicmob/

echo "[DONE]"

echo "Follow next steps on documentation... The dev environment has been setup correctly"

exit 0

