# README

This software is intended to manage the billing area of a small mobile comunications cooperative.


## Dependencies

Please find the correspondent names in your distro. This are intended for debian based dstros:

* `python3`
* `python3-pip`
* `python3-venv`
* `git`
* `postgresql`


## Contributing

Please read our [dedicated documentation](CONTRIBUTING.md) on how to set up a dev environment and main guidelines.


